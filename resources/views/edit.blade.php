<!DOCTYPE html>
<html>
<head>
	<title>Update Data</title>
    <link rel="stylesheet" type="text/css" href="css/style.css">
</head>
<body class="edit">
	@foreach($karyawan as $p)

	<form action="/update" method="post">
		{{ csrf_field() }}
        <input type="hidden" name="id" value="{{ $p->id }}"> <br/>
        Nama <input type="text" required="required" name="nama" value="{{ $p->nama_karyawan }}"> <br/>
		No <input type="number" required="required" name="no" value="{{ $p->no_karyawan }}"> <br/>
		No Telp <input type="text" required="required" name="notlp" value="{{ $p->no_telp_karyawan }}"> <br/>
		Jabtan <input type="text" required="required" name="jabatan" value="{{ $p->jabatan_karyawan }}"> <br/>
        Divisi <input type="text" required="required" name="devisi" value="{{ $p->divisi_karyawan }}"> <br/>
		<input type="submit" value="Simpan Data">
	</form>
	@endforeach
</body>
</html>